# Codon SLURM Training October 2023

## Training Survey

Please take a minute to fill out training Survey. This helps us in better planning our future trainings.

[Survey Link](https://forms.gle/79Gh24wzkuS6GnV29)

## Consultation Slots

If you are interested in having one-on-one consutlation slots with HPC experts, please fill out this form

[Book Consultation Session](https://docs.google.com/spreadsheets/d/1S45epP_yrCisjb5wD_CyMw3yzOE1D5gTLF7I1fFMq2k/edit?usp=sharing)

### Welcome to Codon SLURM Training session at EBI. This is a 2 Day training course on Codon SLURM Cluster. 

## Useful Links
[Day 01 Slides](https://docs.google.com/presentation/d/11_fItN_NxeCs00pFHidDu1EVPn8xcEKRTjMHGw3h8qs/edit?usp=sharing)

[Day 02 Slides](https://docs.google.com/presentation/d/137zRcBahmbWCdAIlXD8uniln0UWFkAOptplUkLTMB6g/edit?usp=sharing)

[Exercises](https://gitlab.ebi.ac.uk/ebi-trainings/slurm-training/-/tree/main/Exercises) (Day 02 exericses will be added tomorrow)

[Codon Cluster Slack Community](https://emblebiglobal.slack.com/archives/C042J8B3F33)

[Training Zoom Link](https://embl-org.zoom.us/j/96585750677)

## How to Get Started?

This course has multiple hands-on exercises.

Instructors will present a section followed by hands-on exercise.

We recommend to only attempt exercise once the relevant section has been presented.

## Step 00: Join SLACK Channel for this training to ask questions
[Training Slack Channel Link](https://emblebiglobal.slack.com/archives/C061217GWTD)

## Step 01: Login to Codon SLURM Cluster
```bash
ssh username@codon-slurm-login.ebi.ac.uk
```

## Step 02: Clone Git Repo
Once you have logged into Codon Cluster, you should clone this git repo
```bash
git clone https://gitlab.ebi.ac.uk/ebi-trainings/slurm-training.git
```

## Step 03: Perform hands-on exercises
Follow instructors slides and do hands-on exercises if any after the each section.
```bash
cd slurm-training
cd Exercises
cd 01
```
